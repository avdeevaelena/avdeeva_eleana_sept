package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (!checkForSize(inputNumbers.size())) {
            throw new CannotBuildPyramidException();
        }

        if (checkForNull(inputNumbers)) {
            throw new CannotBuildPyramidException();
        }

        if (!checkForZero(inputNumbers)) {
            throw new CannotBuildPyramidException();
        }

        inputNumbers.sort(Integer::compareTo);
        List<List<Integer>> matrix = new ArrayList<>();
        int rowCount = (int) getRowCount(inputNumbers.size());
        int chunkSize = 1;
        int from = 0;

        for (int i = 0; i < rowCount; i++) {
            List<Integer> row = new ArrayList<>(inputNumbers.subList(from, from + chunkSize));
            addZero(row);

            for (int j = 0; j < rowCount - i - 1; j++) {
                row.add(j, 0);
                row.add(0);
            }

            matrix.add(row);
            from = from + chunkSize;
            chunkSize = chunkSize + 1;
        }

        return matrix.stream().map(u -> u.stream().mapToInt(i -> i).toArray()).toArray(int[][]::new);
    }

    /**
     * Check number is integer or not.
     *
     * @param count source number.
     * @return true if number is integer.
     */
    private boolean checkForSize(int count) {
        return getRowCount(count) % 1 == 0;
    }

    /**
     * Check list for null value items.
     *
     * @param inputNumbers source list.
     * @return true if list contain null value item.
     */
    private boolean checkForNull(List<Integer> inputNumbers) {
        return inputNumbers.contains(null);
    }

    /**
     * Check list for zero value items.
     *
     * @param inputNumbers source list.
     * @return true if at least one item value not zero.
     */
    private boolean checkForZero(List<Integer> inputNumbers) {
        return inputNumbers.stream().anyMatch(i -> i != 0);
    }

    /**
     * Return count of row by triangular number.
     *
     * @param count triangular number.
     * @return see description.
     */
    private double getRowCount(int count) {
        return (-0.5 + Math.sqrt(0.25 + 2 * count));
    }

    /**
     * Add zero between list items.
     *
     * @param list source list.
     */
    private void addZero(List<Integer> list) {
        for (int i = 1; i < list.size(); i = i + 2) {
            list.add(i, 0);
        }
    }
}