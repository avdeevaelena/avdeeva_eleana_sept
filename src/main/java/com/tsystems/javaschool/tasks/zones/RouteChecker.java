package com.tsystems.javaschool.tasks.zones;


import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class RouteChecker {


    /**
     * Checks whether required zones are connected with each other.
     * By connected we mean that there is a path from any zone to any zone from the requested list.
     * <p>
     * Each zone from initial state may contain a list of it's neighbours. The link is defined as unidirectional,
     * but can be used as bidirectional.
     * For instance, if zone A is connected with B either:
     * - A has link to B
     * - OR B has a link to A
     * - OR both of them have a link to each other
     *
     * @param zoneState        current list of all available zones
     * @param requestedZoneIds zone IDs from request
     * @return true of zones are connected, false otherwise
     */
    public boolean checkRoute(List<Zone> zoneState, List<Integer> requestedZoneIds) {
        boolean correctPath = zoneState.stream().map(Zone::getId).collect(Collectors.toList()).containsAll(requestedZoneIds);
        if (correctPath) {
            List<Zone> requestedPath = requestedZoneIds.stream().map(i -> zoneState.stream().filter(z -> z.getId() == i).findFirst().get())
                    .collect(Collectors.toList());
            return IntStream.range(0, requestedPath.size() - 1).allMatch(idx -> checkConnection(requestedPath.get(idx), requestedPath
                    .get(idx + 1)));
        }
        return false;
    }

    /**
     * Check connection between two zones.
     *
     * @param a first zone.
     * @param b second zone.
     * @return true if zones are connected.
     */
    private boolean checkConnection(Zone a, Zone b) {
        return a.getNeighbours().contains(b.getId()) || b.getNeighbours().contains(a.getId());
    }
}